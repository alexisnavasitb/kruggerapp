package cat.itb.ane.kruggerapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.ane.kruggerapp.Service.ServiceGame;

public class ButtonFragment extends Fragment {

    private MainViewModel mViewModel;

    private MediaPlayer BuffaloPlayer;
    private MediaPlayer ZebraPlayer;
    private MediaPlayer ElephantPlayer;
    private MediaPlayer WarthogPlayer;
    private MediaPlayer CheetahPlayer;
    private MediaPlayer HyenaPlayer;
    private MediaPlayer LionPlayer;
    private MediaPlayer WildebeestPlayer;

    public static ButtonFragment newInstance() {
        return new ButtonFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.button_fragment, container, false);

    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        BuffaloPlayer = MediaPlayer.create(getContext(), R.raw.sonido_bufalo);
        ZebraPlayer = MediaPlayer.create(getContext(), R.raw.sonido_cebra);
        ElephantPlayer = MediaPlayer.create(getContext(), R.raw.sonido_elefante);
        WarthogPlayer = MediaPlayer.create(getContext(), R.raw.sonido_facoquero);
        CheetahPlayer = MediaPlayer.create(getContext(), R.raw.sonido_cheetah);
        HyenaPlayer = MediaPlayer.create(getContext(), R.raw.sonido_hiena);
        LionPlayer = MediaPlayer.create(getContext(), R.raw.sonido_leon);
        WildebeestPlayer = MediaPlayer.create(getContext(), R.raw.sonido_nu);

    }

    @OnClick(R.id.imagenBlackWildbeest)
    public void onBlackWildeBeestClicked() {
        getActivity().startService(new Intent(getContext(), ServiceGame.class));
        if (BuffaloPlayer.isPlaying()) {
            BuffaloPlayer.pause();
            WildebeestPlayer.start();
        } else if (ZebraPlayer.isPlaying()) {
            ZebraPlayer.pause();
            WildebeestPlayer.start();
        } else if (ElephantPlayer.isPlaying()) {
            ElephantPlayer.pause();
            WildebeestPlayer.start();
        } else if (WarthogPlayer.isPlaying()) {
            WarthogPlayer.pause();
            WildebeestPlayer.start();
        } else if (CheetahPlayer.isPlaying()) {
            CheetahPlayer.pause();
            WildebeestPlayer.start();
        } else if (HyenaPlayer.isPlaying()) {
            HyenaPlayer.pause();
            WildebeestPlayer.start();
        } else if (LionPlayer.isPlaying()) {
            LionPlayer.pause();
            WildebeestPlayer.start();
        } else{
            WildebeestPlayer.start();
        }
    }

    @OnClick(R.id.imagenBuffalo)
    public void onBuffaloClicked() {
        getActivity().startService(new Intent(getContext(), ServiceGame.class));
        if (ZebraPlayer.isPlaying()) {
            ZebraPlayer.pause();
            BuffaloPlayer.start();
        } else if (ElephantPlayer.isPlaying()) {
            ElephantPlayer.pause();
            BuffaloPlayer.start();
        } else if (WarthogPlayer.isPlaying()) {
            WarthogPlayer.pause();
            BuffaloPlayer.start();
        } else if (CheetahPlayer.isPlaying()) {
            CheetahPlayer.pause();
            BuffaloPlayer.start();
        } else if (HyenaPlayer.isPlaying()) {
            HyenaPlayer.pause();
            BuffaloPlayer.start();
        } else if (LionPlayer.isPlaying()) {
            LionPlayer.pause();
            BuffaloPlayer.start();
        } else if (WildebeestPlayer.isPlaying()) {
            WildebeestPlayer.pause();
            BuffaloPlayer.start();
        } else{
            BuffaloPlayer.start();
        }
    }

    @OnClick(R.id.imagenCebra)
    public void onCebraClicked() {
        getActivity().startService(new Intent(getContext(), ServiceGame.class));
        if (BuffaloPlayer.isPlaying()) {
            BuffaloPlayer.pause();
            ZebraPlayer.start();
        } else if (ElephantPlayer.isPlaying()) {
            ElephantPlayer.pause();
            ZebraPlayer.start();
        } else if (WarthogPlayer.isPlaying()) {
            WarthogPlayer.pause();
            ZebraPlayer.start();
        } else if (CheetahPlayer.isPlaying()) {
            CheetahPlayer.pause();
            ZebraPlayer.start();
        } else if (HyenaPlayer.isPlaying()) {
            HyenaPlayer.pause();
            ZebraPlayer.start();
        } else if (LionPlayer.isPlaying()) {
            LionPlayer.pause();
            ZebraPlayer.start();
        } else if (WildebeestPlayer.isPlaying()) {
            WildebeestPlayer.pause();
            ZebraPlayer.start();
        } else{
            ZebraPlayer.start();
        }
    }

    @OnClick(R.id.imagenCheetah)
    public void onChettahClicked() {
        getActivity().startService(new Intent(getContext(), ServiceGame.class));
        if (BuffaloPlayer.isPlaying()) {
            BuffaloPlayer.pause();
            CheetahPlayer.start();
        } else if (ElephantPlayer.isPlaying()) {
            ElephantPlayer.pause();
            CheetahPlayer.start();
        } else if (WarthogPlayer.isPlaying()) {
            WarthogPlayer.pause();
            CheetahPlayer.start();
        } else if (ZebraPlayer.isPlaying()) {
            ZebraPlayer.pause();
            CheetahPlayer.start();
        } else if (HyenaPlayer.isPlaying()) {
            HyenaPlayer.pause();
            CheetahPlayer.start();
        } else if (LionPlayer.isPlaying()) {
            LionPlayer.pause();
            CheetahPlayer.start();
        } else if (WildebeestPlayer.isPlaying()) {
            WildebeestPlayer.pause();
            CheetahPlayer.start();
        } else{
            CheetahPlayer.start();
        }
    }

    @OnClick(R.id.imagenElephant)
    public void onElephantClickd() {
        getActivity().startService(new Intent(getContext(), ServiceGame.class));
        if (BuffaloPlayer.isPlaying()) {
            BuffaloPlayer.pause();
            ElephantPlayer.start();
        } else if (CheetahPlayer.isPlaying()) {
            CheetahPlayer.pause();
            ElephantPlayer.start();
        } else if (WarthogPlayer.isPlaying()) {
            WarthogPlayer.pause();
            ElephantPlayer.start();
        } else if (ZebraPlayer.isPlaying()) {
            ZebraPlayer.pause();
            ElephantPlayer.start();
        } else if (HyenaPlayer.isPlaying()) {
            HyenaPlayer.pause();
            ElephantPlayer.start();
        } else if (LionPlayer.isPlaying()) {
            LionPlayer.pause();
            ElephantPlayer.start();
        } else if (WildebeestPlayer.isPlaying()) {
            WildebeestPlayer.pause();
            ElephantPlayer.start();
        } else{
            ElephantPlayer.start();
        }
    }

    @OnClick(R.id.imagenHyena)
    public void onHyenaClicked() {
        getActivity().startService(new Intent(getContext(), ServiceGame.class));
        if (BuffaloPlayer.isPlaying()) {
            BuffaloPlayer.pause();
            HyenaPlayer.start();
        } else if (CheetahPlayer.isPlaying()) {
            CheetahPlayer.pause();
            HyenaPlayer.start();
        } else if (WarthogPlayer.isPlaying()) {
            WarthogPlayer.pause();
            HyenaPlayer.start();
        } else if (ZebraPlayer.isPlaying()) {
            ZebraPlayer.pause();
            HyenaPlayer.start();
        } else if (ElephantPlayer.isPlaying()) {
            ElephantPlayer.pause();
            HyenaPlayer.start();
        } else if (LionPlayer.isPlaying()) {
            LionPlayer.pause();
            HyenaPlayer.start();
        } else if (WildebeestPlayer.isPlaying()) {
            WildebeestPlayer.pause();
            HyenaPlayer.start();
        } else{
            HyenaPlayer.start();
        }
    }

    @OnClick(R.id.imagenLion)
    public void onLionClicked() {
        getActivity().startService(new Intent(getContext(), ServiceGame.class));
        if (BuffaloPlayer.isPlaying()) {
            BuffaloPlayer.pause();
            LionPlayer.start();
        } else if (CheetahPlayer.isPlaying()) {
            CheetahPlayer.pause();
            LionPlayer.start();
        } else if (WarthogPlayer.isPlaying()) {
            WarthogPlayer.pause();
            LionPlayer.start();
        } else if (ZebraPlayer.isPlaying()) {
            ZebraPlayer.pause();
            LionPlayer.start();
        } else if (ElephantPlayer.isPlaying()) {
            ElephantPlayer.pause();
            LionPlayer.start();
        } else if (HyenaPlayer.isPlaying()) {
            HyenaPlayer.pause();
            LionPlayer.start();
        } else if (WildebeestPlayer.isPlaying()) {
            WildebeestPlayer.pause();
            LionPlayer.start();
        } else{
            LionPlayer.start();
        }
    }

    @OnClick(R.id.imagenWarthog)
    public void onWarthogClicked() {
        getActivity().startService(new Intent(getContext(), ServiceGame.class));
        if (BuffaloPlayer.isPlaying()) {
            BuffaloPlayer.pause();
            WarthogPlayer.start();
        } else if (CheetahPlayer.isPlaying()) {
            CheetahPlayer.pause();
            WarthogPlayer.start();
        } else if (LionPlayer.isPlaying()) {
            LionPlayer.pause();
            WarthogPlayer.start();
        } else if (ZebraPlayer.isPlaying()) {
            ZebraPlayer.pause();
            WarthogPlayer.start();
        } else if (ElephantPlayer.isPlaying()) {
            ElephantPlayer.pause();
            WarthogPlayer.start();
        } else if (HyenaPlayer.isPlaying()) {
            HyenaPlayer.pause();
            WarthogPlayer.start();
        } else if (WildebeestPlayer.isPlaying()) {
            WildebeestPlayer.pause();
            WarthogPlayer.start();
        } else{
            WarthogPlayer.start();
        }
    }
}
